package cache

import (
	"math/rand"
	"time"
)

// A VictimFinder decides with block should be evicted
type VictimFinder interface {
	FindVictim(set *Set) *Block
}

// LRUVictimFinder evicts the least recently used block to evict
type LRUVictimFinder struct {
}

// NewLRUVictimFinder returns a newly constructed lru evictor
func NewLRUVictimFinder() *LRUVictimFinder {
	e := new(LRUVictimFinder)
	return e
}

// FindVictim returns the least recently used block in a set
func (e *LRUVictimFinder) FindVictim(set *Set) *Block {
	// First try evicting an empty block
	for _, block := range set.LRUQueue {
		if !block.IsValid && !block.IsLocked {
			return block
		}
	}

	for _, block := range set.LRUQueue {
		if !block.IsLocked {
			return block
		}
	}

	return set.LRUQueue[0]
}

/*******************************************************************************************************/

// RandomVictimFinder selects a random block to evict
type RandomVictimFinder struct {
}

// NewRandomVictimFinder returns a newly constructed random block evictor
func NewRandomVictimFinder() *RandomVictimFinder {
	e := new(RandomVictimFinder)
	return e
}

// FindVictim returns the least recently used block in a set
func (e *RandomVictimFinder) FindVictim(set *Set) *Block {
	unlockedCount := 0
	// First try evicting an empty block
	for _, block := range set.Blocks {
		if !block.IsValid && !block.IsLocked {
			return block
		}
		if !block.IsLocked {
			unlockedCount += 1
		}
	}

	if unlockedCount == 0 {
		return set.LRUQueue[0]
	}

	// Select a random block from the unlocked blocks
	rand.Seed(time.Now().UnixNano())
	ind := rand.Intn(unlockedCount)

	for _, block := range set.Blocks {
		if !block.IsLocked {
			if ind == 0 {
				return block
			}
			ind -= 1
		}
	}

	return set.LRUQueue[0]
}

/******************************************************************************************************************/

// LRUVictimFinder evicts the least recently used block to evict
type LFUVictimFinder struct {
}

// NewLRUVictimFinder returns a newly constructed lru evictor
func NewLFUVictimFinder() *LFUVictimFinder {
	e := new(LFUVictimFinder)
	return e
}

// FindVictim returns the least recently used block in a set
func (e *LFUVictimFinder) FindVictim(set *Set) *Block {
	// First try evicting an empty block
	for _, block := range set.LRUQueue {
		if !block.IsValid && !block.IsLocked {
			return block
		}
	}

	for _, block := range set.LRUQueue {
		if !block.IsLocked {
			return block
		}
	}

	return set.LRUQueue[0]
}
